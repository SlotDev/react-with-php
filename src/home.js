import React, { Component } from 'react';
import axios from 'axios';
class Home extends React.Component{
  constructor(props) {
    super(props);
    this.addFormData = this.addFormData.bind(this);
  }

  addFormData(evt) {
    evt.preventDefault();
    const fd = new FormData();
      fd.append('myUsername', this.refs.myUsername.value);
      fd.append('myEmail', this.refs.myEmail.value);
    var headers = {
      'content-Type': 'application/json;charset=UTF-8',
      "Access-Control-Allow-Origin": "*"
    }
      axios.post('http://localhost/react-test/react-insert/src/api/core_php.php', fd, headers
    ).then(res=> { 
      alert(res.data.data); 
    });
  }

  render() {
    return (
      <div>
        <p>Home</p>
          <h5>Enter Your Info for testing:</h5>

        <form>
          <div className="form-group">
            <input type="email" className="form-control" id="Email" aria-describedby="emailHelp" placeholder="Enter email" ref="myEmail" />
            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div className="form-group">
            <input type="text" className="form-control" id="Username" placeholder="Enter Username" ref="myUsername" />
          </div>
          <button type="submit" className="btn btn-primary" onClick={this.addFormData}>Submit</button>
        </form>
      <br />
      </div>
    );
  }

}// end component
export default Home;