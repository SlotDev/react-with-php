import React, { Component } from 'react';
import ReactDom from 'react-dom';

class ChangeText extends React.Component{
  constructor() {
    super();
      this.state = {
        title: "Click Me"
      }
  }
  render() {
    return (
      <h1 onClick={ () => this.setState({ title: "New title"})}>{this.state.title}</h1>
    )
  }
  
}// end component
export default ChangeText;