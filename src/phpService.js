import React, { Component } from 'react';
import ReactDom from 'react-dom';

class phpService extends React.Component{

  componentWillMount() {
    fetch('http://localhost/react-test/react-upload-img/src/api/request.php')
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
    })
  }

  render() {
    return (
      <div>
        <h1>Call php web service</h1>
        <p>View in console.log</p>
      </div>
    ) 
  }

}// end component
export default phpService;