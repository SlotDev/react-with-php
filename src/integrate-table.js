import React, { Component } from 'react';
import ReactDom from 'react-dom';
import {Table, Column, Cell} from 'fixed-data-table';
import 'fixed-data-table/dist/fixed-data-table.css';

const rows = [
  {
    "id":1, 
    "first_name":"Jane", 
    "last_name":"Post", 
    "email":"jayu.develop@gmail.com", 
    "country":"Thailand"
  },
  {
    "id":2, 
    "first_name":"Au", 
    "last_name":"Post2", 
    "email":"Au.develop@gmail.com", 
    "country":"Thailand"
  },
  {
    "id":3, 
    "first_name":"TheRich", 
    "last_name":"Post3", 
    "email":"TheRich.develop@gmail.com", 
    "country":"Thailand"
  }
];

class integrateTable extends React.Component{
  render() {
    return( 
      <Table
        height={rows.length*50}
        width={1000}
        rowsCount={rows.length}
        rowHeight={30}
        headerHeight={30}
        rowGetter={function(rowIndex) {return rows[rowIndex]; }}>
        
        <Column dataKey="id" width={50} label="id" />
        <Column dataKey="first_name" width={200} label="First Name" />
        <Column dataKey="last_name" width={200} label="Last Name" />
        <Column dataKey="email" width={400} label="E-mail" />
        <Column dataKey="country" width={300} label="Country" />
      </Table>
    )
  }

}// end component
export default integrateTable;