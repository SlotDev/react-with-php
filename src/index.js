import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import Home from './home';
import ImgUpload from './ImgUpload';
import getField from './get-input-field';
import integrateTable from './integrate-table';
import stateArray from './state-array';
import phpService from './phpService';
import ChangeText from './changeText';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
  Row,
  Col,
  Jumbotron,
  Button,
  Modal,
  ModalHeader,
  ModalFooter
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
//import App from './App';
import registerServiceWorker from './registerServiceWorker';

class Main extends React.Component{
  render() {
    return (
      <Router>
        <Container>
          <Row>
            <Col xs="3">
              <h2>React-PHP</h2> 
              <Navbar light expand="md">
                <NavbarBrand></NavbarBrand>
                <Nav vertical navbar>
                  <NavItem>
                    <div>
                      <Link to={'/home'}>Home</Link>
                    </div>
                    <div>
                      <Link to={'/ImgUpload'}>Img_upload</Link>
                    </div>
                    <div>
                      <Link to={'/getField'}>getField_value</Link>
                    </div>
                    <div>
                      <Link to={'/integrateTable'}>integrate_table</Link>
                    </div>
                    <div>
                      <Link to={'/stateArray'}>state_array</Link>
                    </div>
                    <div>
                      <Link to={'/phpService'}>Service_JSON</Link>
                    </div> 
                    <div>
                      <Link to={'/ChangeText'}>Change_text</Link>
                    </div>
                  </NavItem>
                </Nav>
              </Navbar>
            </Col> 

            <Col xs="9">
              <h4><small>RECENT Pages Data</small></h4>
              <hr />
              <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/home' component={Home} />
                <Route exact path='/ImgUpload' component={ImgUpload} />
                <Route exact path='/getField' component={getField} />
                <Route exact path='/integrateTable' component={integrateTable} />
                <Route exact path='/stateArray' component={stateArray} />
                <Route exact path='/phpService' component={phpService} />
                <Route exact path='/ChangeText' component={ChangeText} />
              </Switch>
            </Col>
            
            <footer class="container-fluid">
              <center><p>JAYU.Dev</p></center>
            </footer>

          </Row> 
        </Container> 
      </Router>
    )
  }
}


ReactDOM.render( < Main / > , document.getElementById('root'));
registerServiceWorker();